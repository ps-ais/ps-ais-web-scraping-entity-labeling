from bs4 import BeautifulSoup
import re
import json
import urllib.request

with urllib.request.urlopen('https://www.hs-heilbronn.de/de/alexander.ritzel') as response:
    soup = BeautifulSoup(response, 'html.parser')

# HTML-Code des script-Tags
html = str(soup.find_all('script')[1])
# print(html)

# Link extrahieren
secondary_link = re.search(r'src="(.+)"', html).group(1)
# print(secondary_link)

primary_link = 'https://www.hs-heilbronn.de'
absolute_link = primary_link + secondary_link
# print(absolute_link)
print(secondary_link)
print(absolute_link)
# URL der JSON-Datei
url = absolute_link

# JSON-Datei herunterladen
response = urllib.request.urlopen(url)
data = response.read()
stripped_data = data.strip()[21:-1]
string = stripped_data.decode('utf-8')
# print(string)

# remove all basic errors
cleaned = string.replace("\\", "").replace('"{', "{").replace('}"', "}")

# remove continuation error
continuation_pattern = r',"continuation.+]"}'
r = re.search(continuation_pattern, cleaned)
idx_start, idx_end = r.start(), r.end()

cleaned = cleaned[:idx_start] + cleaned[idx_end - 1:]

# remove csid error
csid_pattern = r'"csid.+]",'
r = re.search(csid_pattern, cleaned)
idx_start, idx_end = r.start(), r.end()

cleaned = cleaned[:idx_start] + cleaned[idx_end:]

json_obj = json.loads(cleaned)

# Beispiel mit der Anzeige der Email
email = json_obj["recording"][0][2]["email"][1]
# email = json_obj["recording"]
print(email)

# parse all texts
pattern = r'"text":\[[^\]]*]\},'
all_texts = re.findall(pattern, cleaned)

# print(all_texts)
from openpyxl.workbook import Workbook
from selenium import webdriver
from openpyxl import load_workbook
from bs4 import BeautifulSoup
import re
import json
import urllib.request
import requests


wb = load_workbook('secondlink.xlsx')
ws = wb.active
# print(ws['B2'].value)
column_a = ws['B']
# print(column_a)
primary_link = 'https://www.hs-heilbronn.de'

results = []
reihenfolge = 1

for cell in column_a:
    # print(f'{primary_link}{cell.value}\n')
    gesamterlink = primary_link+cell.value
    results.append(gesamterlink)
    # print(gesamterlink)
# print(results)
# print(results)

predateien = []
for link in results:
    # Zugriff auf einen Link in der Liste
    linker = results[reihenfolge]

    # Holen Sie sich den Inhalt der Seite
    page = requests.get(linker)

    # print(page)
    with urllib.request.urlopen(linker) as response:
        soup = BeautifulSoup(page.content, 'html.parser')
    # HTML-Code des script-Tags
    html = str(soup.find_all('script')[1])
    secondary_link = re.search(r'src="(.+)"', html).group(1)
    ersterlink = "https://www.hs-heilbronn.de"
    zusammenlink = ersterlink+secondary_link

    # JSON-Datei herunterladen
    response = urllib.request.urlopen(zusammenlink)
    data = response.read()
    stripped_data = data.strip()[21:-1]
    string = stripped_data.decode('utf-8')
    # print(string)

    # remove all basic errors
    cleaned = string.replace("\\", "").replace('"{', "{").replace('}"', "}")

    # remove continuation error
    continuation_pattern = r',"continuation.+]"}'
    r = re.search(continuation_pattern, cleaned)
    idx_start, idx_end = r.start(), r.end()

    cleaned = cleaned[:idx_start] + cleaned[idx_end - 1:]

    # remove csi d error
    csid_pattern = r'"csid.+]",'
    r = re.search(csid_pattern, cleaned)
    idx_start, idx_end = r.start(), r.end()

    cleaned = cleaned[:idx_start] + cleaned[idx_end:]

    json_obj = json.loads(cleaned)
    # Beispiel mit der Anzeige der Email
    email = json_obj["recording"][0][2]["email"][1]
    # email = json_obj["recording"]
    print(email)

    # parse all texts
    pattern = r'"text":\[[^\]]*]\},'
    all_texts = re.findall(pattern, cleaned)

    reihenfolge += 1

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import re
import itertools
import string
import pandas as pd
import openpyxl

# Prozess im Hintergrund laufen lassen
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# driver = webdriver.Chrome(chrome_options=options)

# Prozess im Vordergrund laufen lassen
driver = webdriver.Chrome("\chromedriver.exe")

# Link start über die direkte Suche
driver.get("https://www.hs-heilbronn.de/de/suchergebnisse-4faa73f18366dedf?q=")

# time.sleep um dem Vorgang etwas Zeit zu verschaffen
time.sleep(1)

# Cookies entfernen - Sorgen für Probleme beim Öffnen weiterer Personen
cookies = driver.find_element(By.XPATH, '//button[@class="cookie-button btn btn-secondary ml-auto btn-block"]')
cookies.click()

# Buchstaben, aus denen die Kombinationen erstellt werden sollen
letters = string.ascii_lowercase

# Erstellen einer N-Buchstaben-Kombinationen
combinations = itertools.product(letters, repeat=3)

# Erstellen einer leeren Liste
previous_results = []
# df = pd.DataFrame(columns=["Search Input", "Ergebnisse"])
df = pd.DataFrame(columns=["Ergebnisse"])
# Durchlaufe alle N-Buchstaben-Kombinationen
for combination in combinations:

    # Lupe wird durch XPATH in html gesucht und angeklickt
    searchfield = driver.find_element(By.XPATH, '//i[@class="fa fa-search fa-lg"]')
    searchfield.click()

    time.sleep(1)
    # Zugriff auf das Eingabefeld neben der Lupe durch XPATH in html
    search = driver.find_element(By.XPATH, '//input[@class="search-input focus-visible"]')
    search.click()

    # Eingabe in das Suchfeld der jeweiligen Kombination aus N-Buchstaben
    search_input = "".join(combination)
    search.clear()
    search.send_keys(search_input)

    # Hier kann Code hinzugefügt werden, um die Suche auszuführen
    search.submit()

    # Warte auf die Suchergebnisse
    time.sleep(2)
    reiter = driver.find_element(By.XPATH, '//div[@class="search-filter row"]/button[4]')
    reiter.click()
    time.sleep(1)

####### TESTBEREICH ######
    try:
        element = driver.find_element(By.XPATH, '//div[@class="col text-center"]')
        count = 0
        while True:
            try:
                element.click()
                count += 1
                time.sleep(1)
            except:
                # Code zum Behandeln von Ausnahmen
                pass
            if count >= 10:
                break
    except:
        print("Button not exists, exiting the loop")
    time.sleep(1)

    # Finde das Ergebniselement auf der Webseite
    results = driver.find_elements(By.XPATH, '//div[@class="result-content"]/a')
    elements = driver.execute_script("return document.getElementsByClassName('result-content')")

    if len(elements) > 0:
        test = [e.get_attribute("innerHTML") for e in elements]
        nothing = [re.search(r'href="[^"]*"', a).group().split("href=")[1].replace('"',"") for a in test]
        # df = pd.DataFrame(previous_results, columns=['Professoren'])
        # df.to_excel("professoren.xlsx")
        if nothing not in previous_results:
            # das bereinigte Suchergebnis wird durch .append in die Liste hinzugefügt
            previous_results.append(nothing)
            # mit pandas die vereinzelten Ergebnisse untereinander auflisten
            # df = pd.concat([df, pd.DataFrame({'Search Input': search_input, 'Ergebnisse': nothing})], ignore_index=True)
            df = pd.concat([df, pd.DataFrame({'Ergebnisse': nothing})], ignore_index=True)
            print(search_input, nothing)
    df.to_excel("Ergebnisse.xlsx")
    df = df.drop_duplicates(subset=["Ergebnisse"])

# Schließe den Browser
driver.quit()

# Browser nach dem Vorgang nochmal etwas Zeit geben falls wir noch einen Blick drauf werfen wollen
time.sleep(5)
